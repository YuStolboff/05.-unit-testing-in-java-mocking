#Practice

##Task 3. Mocking       

Checkout https://bitbucket.org/alexandermrd/junitmocking.git  
Implement tests for PaymentController class.  
Mock Account and DepositService for it using Mockito (use @InjectMocks annotation).  
Do not implement services themselves, test using mocks.  
Configure mocks that for user with id 100 isAuthenticated will return “true”.

For deposit of amount less than hundred transaction (any userId) will be successful,  
any other – will throw InsufficientFundsException  (use Mockito AdditionalMatchers).

Mock initialisation should be done in one place, for each test.