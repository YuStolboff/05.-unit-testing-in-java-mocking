package edu.epam.izhevsk.junit;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.AdditionalMatchers.gt;
import static org.mockito.AdditionalMatchers.lt;
import static org.mockito.AdditionalMatchers.or;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

public class PaymentControllerTest {

    private static final long MIN_DEPOSIT = 1L;
    private static final long MAX_DEPOSIT = 100L;
    private static final long CORRECT_ID = 100L;
    private static final long INCORRECT_ID = 200L;
    private static final long CORRECT_DEPOSIT = 50L;
    private static final long INCORRECT_DEPOSIT = 500L;

    @Mock
    private AccountService accountService;
    @Mock
    private DepositService depositService;
    @InjectMocks
    private PaymentController paymentController;

    @Before
    public void init() throws InsufficientFundsException {
        MockitoAnnotations.initMocks(this);
        when(accountService.isUserAuthenticated(CORRECT_ID)).thenReturn(true).thenReturn(false);
        when(depositService.deposit(or(lt(MIN_DEPOSIT), gt(MAX_DEPOSIT)), anyLong()))
                .thenThrow(new InsufficientFundsException());
    }

    @Test
    public void testSuccessFullDeposit() throws InsufficientFundsException {
        paymentController.deposit(CORRECT_DEPOSIT, CORRECT_ID);
    }

    @Test(expected = InsufficientFundsException.class)
    public void testFailedDepositOfLargeAmountException() throws InsufficientFundsException {
        paymentController.deposit(INCORRECT_DEPOSIT, CORRECT_ID);
    }

    @Test(expected = SecurityException.class)
    public void testFailedDepositForUnauthenticatedUser() throws SecurityException, InsufficientFundsException {
        paymentController.deposit(MAX_DEPOSIT, INCORRECT_ID);
    }
}
